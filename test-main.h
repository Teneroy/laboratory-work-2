#ifndef _TESTMAIN_H_
#define _TESTMAIN_H_

#include "base-types.h"
#include "shape.h"
#include "Rectangle.h"
#include "Circle.h"
#include <stdio.h>
#include <iostream>

bool testConstWidth();

bool testSqrtArea();

bool testUncorrectParam();

#endif