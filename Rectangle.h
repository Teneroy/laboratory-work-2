#ifndef _RECTANGLE_H_
#define _RECTANGLE_H_

#include "shape.h"
#include "base-types.h"

class Rectangle: public Shape {
public: 
	Rectangle();
	Rectangle(rectangle_t rectangle);
	Rectangle(const Rectangle &obj);
	~Rectangle();
	float getArea() const;
	rectangle_t getFrameRect();
	void move(float x, float y);
	void move(point_t movepos);
	void scale(float coeff);
private: 
	rectangle_t m_rect;
};

#endif