#ifndef _SHAPE_H_
#define _SHAPE_H_
#include "base-types.h"

class Shape {
public: 
	virtual float getArea() const = 0;
	virtual rectangle_t getFrameRect() = 0;
	virtual void move(float x, float y) = 0;	
	virtual void move(point_t movepos) = 0;
	virtual void scale(float coeff) = 0;
};

#endif