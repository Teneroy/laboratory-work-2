#include "test-main.h"
#include <iostream>
#include <cassert>
using namespace std;

bool testConstWidth() {

	point_t position; 
	position.x = 0.232;
	position.y = 1.343;

	rectangle_t rectangle;
	rectangle.width = 2.5;
	rectangle.height = 3.5;
	rectangle.pos = position;

	Shape *testRectangle[1];

	testRectangle[0] = new Rectangle (rectangle);

	testRectangle[0] -> move(2.345, 3.4454);

	rectangle_t movedRect = testRectangle[0] -> getFrameRect();

	printf("%f\n", movedRect.pos.x);

	if(movedRect.width == rectangle.width && movedRect.height == rectangle.height) {
		return true;
	} else {
		return false;
	}

}

bool testSqrtArea() {

	point_t position; 
	position.x = 0.232;
	position.y = 1.343;

	rectangle_t rectangle;
	rectangle.width = 2.5;
	rectangle.height = 3.5;
	rectangle.pos = position;

	circle_t circle;
	circle.radius = 6.5;
	circle.pos = position;

	float rectCoeff = rectangle.width / rectangle.height;
	float radCoeff = circle.radius / circle.radius;

	Shape *testShape[2];

	testShape[0] = new Rectangle (rectangle);
	testShape[0] -> scale(0.25);
	rectangle_t scaleRect = testShape[0] -> getFrameRect();

	testShape[1] = new Circle (circle);
	testShape[1] -> scale(0.25);
	rectangle_t scaleCirc = testShape[1] -> getFrameRect();

	float scaleRadCoeff = scaleCirc.width / scaleCirc.height;
	float scaleRectCoeff = scaleRect.width / scaleRect.height;

	if((scaleRadCoeff == radCoeff) && (scaleRectCoeff == rectCoeff)) {
		return true;
	} else {
		return false;
	}

}

bool testUncorrectParam() {

	point_t position; 
	position.x = 0.232;
	position.y = 1.343;

	rectangle_t rectangle;
	rectangle.width = -2.5;
	rectangle.height = -3.5;
	rectangle.pos = position;

	Shape *testRectangle[1];

	try {
		testRectangle[0] = new Rectangle (rectangle);
	} catch(exception& e) {
		e.what();
	}

	

}