#ifndef _BASETYPES_H_
#define _BASETYPES_H_

struct point_t 
{
	float x,y;
};

struct rectangle_t
{
	float width,height;
	point_t pos;
};

struct circle_t
{
	float radius;
	point_t pos;
};

#endif